// Generated from /home/abolotov/projects/antlr-with-spring-data/src/main/java/ru/example/antlr/antlrwithspringdata/lang/AntlrQueryParser.g4 by ANTLR 4.9.1
package antlr.gen;

import  java.util.*;

import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link AntlrQueryParser}.
 */
public interface AntlrQueryParserListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link AntlrQueryParser#query}.
	 * @param ctx the parse tree
	 */
	void enterQuery(AntlrQueryParser.QueryContext ctx);
	/**
	 * Exit a parse tree produced by {@link AntlrQueryParser#query}.
	 * @param ctx the parse tree
	 */
	void exitQuery(AntlrQueryParser.QueryContext ctx);
	/**
	 * Enter a parse tree produced by {@link AntlrQueryParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterExpression(AntlrQueryParser.ExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link AntlrQueryParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitExpression(AntlrQueryParser.ExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link AntlrQueryParser#parameter}.
	 * @param ctx the parse tree
	 */
	void enterParameter(AntlrQueryParser.ParameterContext ctx);
	/**
	 * Exit a parse tree produced by {@link AntlrQueryParser#parameter}.
	 * @param ctx the parse tree
	 */
	void exitParameter(AntlrQueryParser.ParameterContext ctx);
	/**
	 * Enter a parse tree produced by {@link AntlrQueryParser#dataEntity}.
	 * @param ctx the parse tree
	 */
	void enterDataEntity(AntlrQueryParser.DataEntityContext ctx);
	/**
	 * Exit a parse tree produced by {@link AntlrQueryParser#dataEntity}.
	 * @param ctx the parse tree
	 */
	void exitDataEntity(AntlrQueryParser.DataEntityContext ctx);
	/**
	 * Enter a parse tree produced by {@link AntlrQueryParser#definition}.
	 * @param ctx the parse tree
	 */
	void enterDefinition(AntlrQueryParser.DefinitionContext ctx);
	/**
	 * Exit a parse tree produced by {@link AntlrQueryParser#definition}.
	 * @param ctx the parse tree
	 */
	void exitDefinition(AntlrQueryParser.DefinitionContext ctx);
	/**
	 * Enter a parse tree produced by {@link AntlrQueryParser#command}.
	 * @param ctx the parse tree
	 */
	void enterCommand(AntlrQueryParser.CommandContext ctx);
	/**
	 * Exit a parse tree produced by {@link AntlrQueryParser#command}.
	 * @param ctx the parse tree
	 */
	void exitCommand(AntlrQueryParser.CommandContext ctx);
	/**
	 * Enter a parse tree produced by {@link AntlrQueryParser#condition}.
	 * @param ctx the parse tree
	 */
	void enterCondition(AntlrQueryParser.ConditionContext ctx);
	/**
	 * Exit a parse tree produced by {@link AntlrQueryParser#condition}.
	 * @param ctx the parse tree
	 */
	void exitCondition(AntlrQueryParser.ConditionContext ctx);
	/**
	 * Enter a parse tree produced by {@link AntlrQueryParser#returnexpr}.
	 * @param ctx the parse tree
	 */
	void enterReturnexpr(AntlrQueryParser.ReturnexprContext ctx);
	/**
	 * Exit a parse tree produced by {@link AntlrQueryParser#returnexpr}.
	 * @param ctx the parse tree
	 */
	void exitReturnexpr(AntlrQueryParser.ReturnexprContext ctx);
	/**
	 * Enter a parse tree produced by {@link AntlrQueryParser#variable}.
	 * @param ctx the parse tree
	 */
	void enterVariable(AntlrQueryParser.VariableContext ctx);
	/**
	 * Exit a parse tree produced by {@link AntlrQueryParser#variable}.
	 * @param ctx the parse tree
	 */
	void exitVariable(AntlrQueryParser.VariableContext ctx);
	/**
	 * Enter a parse tree produced by {@link AntlrQueryParser#newVariable}.
	 * @param ctx the parse tree
	 */
	void enterNewVariable(AntlrQueryParser.NewVariableContext ctx);
	/**
	 * Exit a parse tree produced by {@link AntlrQueryParser#newVariable}.
	 * @param ctx the parse tree
	 */
	void exitNewVariable(AntlrQueryParser.NewVariableContext ctx);
	/**
	 * Enter a parse tree produced by {@link AntlrQueryParser#methodInvoke}.
	 * @param ctx the parse tree
	 */
	void enterMethodInvoke(AntlrQueryParser.MethodInvokeContext ctx);
	/**
	 * Exit a parse tree produced by {@link AntlrQueryParser#methodInvoke}.
	 * @param ctx the parse tree
	 */
	void exitMethodInvoke(AntlrQueryParser.MethodInvokeContext ctx);
}