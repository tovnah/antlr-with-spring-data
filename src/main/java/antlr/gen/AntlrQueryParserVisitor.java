// Generated from /home/abolotov/projects/antlr-with-spring-data/src/main/java/ru/example/antlr/antlrwithspringdata/lang/AntlrQueryParser.g4 by ANTLR 4.9.1
package antlr.gen;

import  java.util.*;

import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link AntlrQueryParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface AntlrQueryParserVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link AntlrQueryParser#query}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitQuery(AntlrQueryParser.QueryContext ctx);
	/**
	 * Visit a parse tree produced by {@link AntlrQueryParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpression(AntlrQueryParser.ExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link AntlrQueryParser#parameter}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParameter(AntlrQueryParser.ParameterContext ctx);
	/**
	 * Visit a parse tree produced by {@link AntlrQueryParser#dataEntity}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDataEntity(AntlrQueryParser.DataEntityContext ctx);
	/**
	 * Visit a parse tree produced by {@link AntlrQueryParser#definition}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDefinition(AntlrQueryParser.DefinitionContext ctx);
	/**
	 * Visit a parse tree produced by {@link AntlrQueryParser#command}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCommand(AntlrQueryParser.CommandContext ctx);
	/**
	 * Visit a parse tree produced by {@link AntlrQueryParser#condition}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCondition(AntlrQueryParser.ConditionContext ctx);
	/**
	 * Visit a parse tree produced by {@link AntlrQueryParser#returnexpr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitReturnexpr(AntlrQueryParser.ReturnexprContext ctx);
	/**
	 * Visit a parse tree produced by {@link AntlrQueryParser#variable}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVariable(AntlrQueryParser.VariableContext ctx);
	/**
	 * Visit a parse tree produced by {@link AntlrQueryParser#newVariable}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNewVariable(AntlrQueryParser.NewVariableContext ctx);
	/**
	 * Visit a parse tree produced by {@link AntlrQueryParser#methodInvoke}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMethodInvoke(AntlrQueryParser.MethodInvokeContext ctx);
}