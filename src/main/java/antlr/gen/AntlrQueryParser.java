// Generated from /home/abolotov/projects/antlr-with-spring-data/src/main/java/ru/example/antlr/antlrwithspringdata/lang/AntlrQueryParser.g4 by ANTLR 4.9.1
package antlr.gen;

import  java.util.*;

import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class AntlrQueryParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.9.1", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		NL=1, WHITESPACE=2, END_OF_EXPR=3, DOT=4, EQEQ=5, LPAREN=6, RPAREN=7, 
		ASSIGNMENT=8, VAR=9, RETURN=10, SPRDTCRSR=11, PARAMCRSR=12, WORD=13;
	public static final int
		RULE_query = 0, RULE_expression = 1, RULE_parameter = 2, RULE_dataEntity = 3, 
		RULE_definition = 4, RULE_command = 5, RULE_condition = 6, RULE_returnexpr = 7, 
		RULE_variable = 8, RULE_newVariable = 9, RULE_methodInvoke = 10;
	private static String[] makeRuleNames() {
		return new String[] {
			"query", "expression", "parameter", "dataEntity", "definition", "command", 
			"condition", "returnexpr", "variable", "newVariable", "methodInvoke"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, null, null, "';'", "'.'", "'=='", "'('", "')'", "'='", "'var'", 
			"'return'", "'#'", "'$'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, "NL", "WHITESPACE", "END_OF_EXPR", "DOT", "EQEQ", "LPAREN", "RPAREN", 
			"ASSIGNMENT", "VAR", "RETURN", "SPRDTCRSR", "PARAMCRSR", "WORD"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "AntlrQueryParser.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public AntlrQueryParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	public static class QueryContext extends ParserRuleContext {
		public ReturnexprContext returnexpr() {
			return getRuleContext(ReturnexprContext.class,0);
		}
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public QueryContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_query; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AntlrQueryParserListener ) ((AntlrQueryParserListener)listener).enterQuery(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AntlrQueryParserListener ) ((AntlrQueryParserListener)listener).exitQuery(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AntlrQueryParserVisitor ) return ((AntlrQueryParserVisitor<? extends T>)visitor).visitQuery(this);
			else return visitor.visitChildren(this);
		}
	}

	public final QueryContext query() throws RecognitionException {
		QueryContext _localctx = new QueryContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_query);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(25);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==VAR || _la==WORD) {
				{
				{
				setState(22);
				expression();
				}
				}
				setState(27);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(28);
			returnexpr();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpressionContext extends ParserRuleContext {
		public TerminalNode ASSIGNMENT() { return getToken(AntlrQueryParser.ASSIGNMENT, 0); }
		public TerminalNode END_OF_EXPR() { return getToken(AntlrQueryParser.END_OF_EXPR, 0); }
		public NewVariableContext newVariable() {
			return getRuleContext(NewVariableContext.class,0);
		}
		public VariableContext variable() {
			return getRuleContext(VariableContext.class,0);
		}
		public CommandContext command() {
			return getRuleContext(CommandContext.class,0);
		}
		public DefinitionContext definition() {
			return getRuleContext(DefinitionContext.class,0);
		}
		public ExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AntlrQueryParserListener ) ((AntlrQueryParserListener)listener).enterExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AntlrQueryParserListener ) ((AntlrQueryParserListener)listener).exitExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AntlrQueryParserVisitor ) return ((AntlrQueryParserVisitor<? extends T>)visitor).visitExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExpressionContext expression() throws RecognitionException {
		ExpressionContext _localctx = new ExpressionContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_expression);
		try {
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(32);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,1,_ctx) ) {
			case 1:
				{
				setState(30);
				newVariable();
				}
				break;
			case 2:
				{
				setState(31);
				variable();
				}
				break;
			}
			setState(34);
			match(ASSIGNMENT);
			setState(37);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,2,_ctx) ) {
			case 1:
				{
				setState(35);
				command();
				}
				break;
			case 2:
				{
				setState(36);
				definition();
				}
				break;
			}
			setState(39);
			match(END_OF_EXPR);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ParameterContext extends ParserRuleContext {
		public TerminalNode PARAMCRSR() { return getToken(AntlrQueryParser.PARAMCRSR, 0); }
		public TerminalNode WORD() { return getToken(AntlrQueryParser.WORD, 0); }
		public ParameterContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_parameter; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AntlrQueryParserListener ) ((AntlrQueryParserListener)listener).enterParameter(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AntlrQueryParserListener ) ((AntlrQueryParserListener)listener).exitParameter(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AntlrQueryParserVisitor ) return ((AntlrQueryParserVisitor<? extends T>)visitor).visitParameter(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ParameterContext parameter() throws RecognitionException {
		ParameterContext _localctx = new ParameterContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_parameter);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(41);
			match(PARAMCRSR);
			setState(42);
			match(WORD);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DataEntityContext extends ParserRuleContext {
		public TerminalNode SPRDTCRSR() { return getToken(AntlrQueryParser.SPRDTCRSR, 0); }
		public TerminalNode WORD() { return getToken(AntlrQueryParser.WORD, 0); }
		public DataEntityContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_dataEntity; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AntlrQueryParserListener ) ((AntlrQueryParserListener)listener).enterDataEntity(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AntlrQueryParserListener ) ((AntlrQueryParserListener)listener).exitDataEntity(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AntlrQueryParserVisitor ) return ((AntlrQueryParserVisitor<? extends T>)visitor).visitDataEntity(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DataEntityContext dataEntity() throws RecognitionException {
		DataEntityContext _localctx = new DataEntityContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_dataEntity);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(44);
			match(SPRDTCRSR);
			setState(45);
			match(WORD);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DefinitionContext extends ParserRuleContext {
		public MethodInvokeContext methodInvoke() {
			return getRuleContext(MethodInvokeContext.class,0);
		}
		public ParameterContext parameter() {
			return getRuleContext(ParameterContext.class,0);
		}
		public DataEntityContext dataEntity() {
			return getRuleContext(DataEntityContext.class,0);
		}
		public VariableContext variable() {
			return getRuleContext(VariableContext.class,0);
		}
		public DefinitionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_definition; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AntlrQueryParserListener ) ((AntlrQueryParserListener)listener).enterDefinition(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AntlrQueryParserListener ) ((AntlrQueryParserListener)listener).exitDefinition(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AntlrQueryParserVisitor ) return ((AntlrQueryParserVisitor<? extends T>)visitor).visitDefinition(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DefinitionContext definition() throws RecognitionException {
		DefinitionContext _localctx = new DefinitionContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_definition);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(50);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case PARAMCRSR:
				{
				setState(47);
				parameter();
				}
				break;
			case SPRDTCRSR:
				{
				setState(48);
				dataEntity();
				}
				break;
			case WORD:
				{
				setState(49);
				variable();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			setState(52);
			methodInvoke();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CommandContext extends ParserRuleContext {
		public TerminalNode WORD() { return getToken(AntlrQueryParser.WORD, 0); }
		public TerminalNode LPAREN() { return getToken(AntlrQueryParser.LPAREN, 0); }
		public ConditionContext condition() {
			return getRuleContext(ConditionContext.class,0);
		}
		public TerminalNode RPAREN() { return getToken(AntlrQueryParser.RPAREN, 0); }
		public CommandContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_command; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AntlrQueryParserListener ) ((AntlrQueryParserListener)listener).enterCommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AntlrQueryParserListener ) ((AntlrQueryParserListener)listener).exitCommand(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AntlrQueryParserVisitor ) return ((AntlrQueryParserVisitor<? extends T>)visitor).visitCommand(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CommandContext command() throws RecognitionException {
		CommandContext _localctx = new CommandContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_command);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(54);
			match(WORD);
			setState(55);
			match(LPAREN);
			setState(56);
			condition();
			setState(57);
			match(RPAREN);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConditionContext extends ParserRuleContext {
		public TerminalNode EQEQ() { return getToken(AntlrQueryParser.EQEQ, 0); }
		public List<DefinitionContext> definition() {
			return getRuleContexts(DefinitionContext.class);
		}
		public DefinitionContext definition(int i) {
			return getRuleContext(DefinitionContext.class,i);
		}
		public List<VariableContext> variable() {
			return getRuleContexts(VariableContext.class);
		}
		public VariableContext variable(int i) {
			return getRuleContext(VariableContext.class,i);
		}
		public List<ParameterContext> parameter() {
			return getRuleContexts(ParameterContext.class);
		}
		public ParameterContext parameter(int i) {
			return getRuleContext(ParameterContext.class,i);
		}
		public ConditionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_condition; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AntlrQueryParserListener ) ((AntlrQueryParserListener)listener).enterCondition(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AntlrQueryParserListener ) ((AntlrQueryParserListener)listener).exitCondition(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AntlrQueryParserVisitor ) return ((AntlrQueryParserVisitor<? extends T>)visitor).visitCondition(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ConditionContext condition() throws RecognitionException {
		ConditionContext _localctx = new ConditionContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_condition);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(62);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,4,_ctx) ) {
			case 1:
				{
				setState(59);
				definition();
				}
				break;
			case 2:
				{
				setState(60);
				variable();
				}
				break;
			case 3:
				{
				setState(61);
				parameter();
				}
				break;
			}
			setState(64);
			match(EQEQ);
			setState(68);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,5,_ctx) ) {
			case 1:
				{
				setState(65);
				definition();
				}
				break;
			case 2:
				{
				setState(66);
				variable();
				}
				break;
			case 3:
				{
				setState(67);
				parameter();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ReturnexprContext extends ParserRuleContext {
		public TerminalNode RETURN() { return getToken(AntlrQueryParser.RETURN, 0); }
		public VariableContext variable() {
			return getRuleContext(VariableContext.class,0);
		}
		public ReturnexprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_returnexpr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AntlrQueryParserListener ) ((AntlrQueryParserListener)listener).enterReturnexpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AntlrQueryParserListener ) ((AntlrQueryParserListener)listener).exitReturnexpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AntlrQueryParserVisitor ) return ((AntlrQueryParserVisitor<? extends T>)visitor).visitReturnexpr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ReturnexprContext returnexpr() throws RecognitionException {
		ReturnexprContext _localctx = new ReturnexprContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_returnexpr);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(70);
			match(RETURN);
			setState(71);
			variable();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class VariableContext extends ParserRuleContext {
		public TerminalNode WORD() { return getToken(AntlrQueryParser.WORD, 0); }
		public VariableContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_variable; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AntlrQueryParserListener ) ((AntlrQueryParserListener)listener).enterVariable(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AntlrQueryParserListener ) ((AntlrQueryParserListener)listener).exitVariable(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AntlrQueryParserVisitor ) return ((AntlrQueryParserVisitor<? extends T>)visitor).visitVariable(this);
			else return visitor.visitChildren(this);
		}
	}

	public final VariableContext variable() throws RecognitionException {
		VariableContext _localctx = new VariableContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_variable);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(73);
			match(WORD);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NewVariableContext extends ParserRuleContext {
		public TerminalNode WORD() { return getToken(AntlrQueryParser.WORD, 0); }
		public TerminalNode VAR() { return getToken(AntlrQueryParser.VAR, 0); }
		public NewVariableContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_newVariable; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AntlrQueryParserListener ) ((AntlrQueryParserListener)listener).enterNewVariable(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AntlrQueryParserListener ) ((AntlrQueryParserListener)listener).exitNewVariable(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AntlrQueryParserVisitor ) return ((AntlrQueryParserVisitor<? extends T>)visitor).visitNewVariable(this);
			else return visitor.visitChildren(this);
		}
	}

	public final NewVariableContext newVariable() throws RecognitionException {
		NewVariableContext _localctx = new NewVariableContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_newVariable);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(76);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==VAR) {
				{
				setState(75);
				match(VAR);
				}
			}

			setState(78);
			match(WORD);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MethodInvokeContext extends ParserRuleContext {
		public TerminalNode DOT() { return getToken(AntlrQueryParser.DOT, 0); }
		public TerminalNode WORD() { return getToken(AntlrQueryParser.WORD, 0); }
		public TerminalNode LPAREN() { return getToken(AntlrQueryParser.LPAREN, 0); }
		public TerminalNode RPAREN() { return getToken(AntlrQueryParser.RPAREN, 0); }
		public MethodInvokeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_methodInvoke; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AntlrQueryParserListener ) ((AntlrQueryParserListener)listener).enterMethodInvoke(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AntlrQueryParserListener ) ((AntlrQueryParserListener)listener).exitMethodInvoke(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof AntlrQueryParserVisitor ) return ((AntlrQueryParserVisitor<? extends T>)visitor).visitMethodInvoke(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MethodInvokeContext methodInvoke() throws RecognitionException {
		MethodInvokeContext _localctx = new MethodInvokeContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_methodInvoke);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(80);
			match(DOT);
			setState(81);
			match(WORD);
			setState(82);
			match(LPAREN);
			setState(83);
			match(RPAREN);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\17X\4\2\t\2\4\3\t"+
		"\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4"+
		"\f\t\f\3\2\7\2\32\n\2\f\2\16\2\35\13\2\3\2\3\2\3\3\3\3\5\3#\n\3\3\3\3"+
		"\3\3\3\5\3(\n\3\3\3\3\3\3\4\3\4\3\4\3\5\3\5\3\5\3\6\3\6\3\6\5\6\65\n\6"+
		"\3\6\3\6\3\7\3\7\3\7\3\7\3\7\3\b\3\b\3\b\5\bA\n\b\3\b\3\b\3\b\3\b\5\b"+
		"G\n\b\3\t\3\t\3\t\3\n\3\n\3\13\5\13O\n\13\3\13\3\13\3\f\3\f\3\f\3\f\3"+
		"\f\3\f\2\2\r\2\4\6\b\n\f\16\20\22\24\26\2\2\2V\2\33\3\2\2\2\4\"\3\2\2"+
		"\2\6+\3\2\2\2\b.\3\2\2\2\n\64\3\2\2\2\f8\3\2\2\2\16@\3\2\2\2\20H\3\2\2"+
		"\2\22K\3\2\2\2\24N\3\2\2\2\26R\3\2\2\2\30\32\5\4\3\2\31\30\3\2\2\2\32"+
		"\35\3\2\2\2\33\31\3\2\2\2\33\34\3\2\2\2\34\36\3\2\2\2\35\33\3\2\2\2\36"+
		"\37\5\20\t\2\37\3\3\2\2\2 #\5\24\13\2!#\5\22\n\2\" \3\2\2\2\"!\3\2\2\2"+
		"#$\3\2\2\2$\'\7\n\2\2%(\5\f\7\2&(\5\n\6\2\'%\3\2\2\2\'&\3\2\2\2()\3\2"+
		"\2\2)*\7\5\2\2*\5\3\2\2\2+,\7\16\2\2,-\7\17\2\2-\7\3\2\2\2./\7\r\2\2/"+
		"\60\7\17\2\2\60\t\3\2\2\2\61\65\5\6\4\2\62\65\5\b\5\2\63\65\5\22\n\2\64"+
		"\61\3\2\2\2\64\62\3\2\2\2\64\63\3\2\2\2\65\66\3\2\2\2\66\67\5\26\f\2\67"+
		"\13\3\2\2\289\7\17\2\29:\7\b\2\2:;\5\16\b\2;<\7\t\2\2<\r\3\2\2\2=A\5\n"+
		"\6\2>A\5\22\n\2?A\5\6\4\2@=\3\2\2\2@>\3\2\2\2@?\3\2\2\2AB\3\2\2\2BF\7"+
		"\7\2\2CG\5\n\6\2DG\5\22\n\2EG\5\6\4\2FC\3\2\2\2FD\3\2\2\2FE\3\2\2\2G\17"+
		"\3\2\2\2HI\7\f\2\2IJ\5\22\n\2J\21\3\2\2\2KL\7\17\2\2L\23\3\2\2\2MO\7\13"+
		"\2\2NM\3\2\2\2NO\3\2\2\2OP\3\2\2\2PQ\7\17\2\2Q\25\3\2\2\2RS\7\6\2\2ST"+
		"\7\17\2\2TU\7\b\2\2UV\7\t\2\2V\27\3\2\2\2\t\33\"\'\64@FN";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}