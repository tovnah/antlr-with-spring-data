package antlr.gen;

import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.RuleNode;
import org.antlr.v4.runtime.tree.TerminalNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

@Service
public class AntlrQueryParserVisitorImpl<T>
        implements AntlrQueryParserVisitor<Object> {
    @Override
    public Object visitQuery(AntlrQueryParser.QueryContext ctx) {
        for (AntlrQueryParser.ExpressionContext expressionContext : ctx.expression()) {
            visitExpression(expressionContext);
        }

        Object returnexpr = visitReturnexpr(ctx.returnexpr());

        return returnexpr;
    }

    @Override
    public Object visitExpression(AntlrQueryParser.ExpressionContext ctx) {
        String variableName = visitNewVariable(ctx.newVariable());
        Object definitionResult = visitDefinition(ctx.definition());

        variables.put(variableName, definitionResult);

        return null;
    }

    @Override
    public Object visitParameter(AntlrQueryParser.ParameterContext ctx) {
        return null;
    }

    @Override
    public Object visitDataEntity(AntlrQueryParser.DataEntityContext ctx) {
        return null;
    }

    @Override
    public Object visitDefinition(AntlrQueryParser.DefinitionContext ctx) {
        String entityName = ctx.dataEntity().WORD().getText();
        Object entityRepo = getEntityRepo(entityName);
        dataEntities.put(
                entityName,
                entityRepo
        );

        String methodName = ctx.methodInvoke().WORD().getText();

        Method method = ReflectionUtils.findMethod(entityRepo.getClass(), methodName);
        Assert.notNull(method, "Method wasn't found " + ctx.getParent().getText());

        Object result = ReflectionUtils.invokeMethod(method, entityRepo);

        return result;
    }

    private Object getEntityRepo(String entityName) {
        return applicationContext.getBean(entityName + "Repository");
    }

    @Override
    public Object visitCommand(AntlrQueryParser.CommandContext ctx) {
        return null;
    }

    @Override
    public Object visitCondition(AntlrQueryParser.ConditionContext ctx) {
        return null;
    }

    @Override
    public Object visitReturnexpr(AntlrQueryParser.ReturnexprContext ctx) {
        return variables.get(ctx.variable().getText());
    }

    @Override
    public Object visitVariable(AntlrQueryParser.VariableContext ctx) {
        return null;
    }

    @Override
    public String visitNewVariable(AntlrQueryParser.NewVariableContext ctx) {
        variables.put(ctx.WORD().getText(), null);

        return ctx.WORD().getText();
    }

    @Override
    public Object visitMethodInvoke(AntlrQueryParser.MethodInvokeContext ctx) {
        return null;
    }

    @Override
    public Object visit(ParseTree parseTree) {
        return null;
    }

    @Override
    public Object visitChildren(RuleNode ruleNode) {
        return null;
    }

    @Override
    public Object visitTerminal(TerminalNode terminalNode) {
        return null;
    }

    @Override
    public Object visitErrorNode(ErrorNode errorNode) {
        return null;
    }

    private ThreadLocal<Map<String, Object>> params;

    public void setContext(Map<String, Object> params) {
        this.params.set(params);
    }

    private Map<String, Object> variables = new HashMap<>();
    private Map<String, Object> dataEntities = new HashMap<>();
    @Autowired
    private ApplicationContext applicationContext;

    public void setApplicationContext(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }
}
