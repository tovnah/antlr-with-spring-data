package ru.example.antlr.antlrwithspringdata;

import ru.example.antlr.antlrwithspringdata.lib.AntlrQuery;
import ru.example.antlr.antlrwithspringdata.lib.AntlrService;

import java.util.List;

@AntlrService
public interface AntlrExampleExecutor {
    String antlrQuery = "var dates = #testEntity.findAll(); return filter $dates.getField() == $param;";

    //    @AntlrQuery("var dates = #testEntity.findAll(); return filter $dates.getField() == $param;")
    @AntlrQuery("var dates = #testEntity.findAll(); return dates;")
    List<TestEntity> filterAnswer(String param);
}

