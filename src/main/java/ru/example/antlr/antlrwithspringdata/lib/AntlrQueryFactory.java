package ru.example.antlr.antlrwithspringdata.lib;


import lombok.SneakyThrows;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Proxy;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class AntlrQueryFactory {
    public static <T> T prepare(Class<T> o, AntlrServiceExecutor antlrServiceExecutor) {
        Map<String, AntlrQueryConfig> configs = new HashMap<>();

        Arrays.stream(ReflectionUtils.getAllDeclaredMethods(o))
                .filter(method -> method.getAnnotation(AntlrQuery.class) != null)
                .forEach(method -> {
                    AntlrQuery antlrQueryAnnotation = method.getAnnotation(AntlrQuery.class);
                    String query = antlrQueryAnnotation.value();
                    String methodName = method.getName();
                    Class<?> returnType = method.getReturnType();
                    configs.put(methodName, new AntlrQueryConfig(o, methodName, query, returnType));
                });

        return createProxy(configs, o, antlrServiceExecutor);
    }

    @SneakyThrows
    private static <T> T createProxy(Map<String, AntlrQueryConfig> configs, Class<T> o, AntlrServiceExecutor antlrServiceExecutor) {
        ClassLoader classLoader = o.getClassLoader();
        Class<?>[] aClass = {classLoader.loadClass(o.getName())};

        AntlrQueryInvocationHandler invocationHandler = new AntlrQueryInvocationHandler(configs);
        invocationHandler.setAntlrServiceExecutor(antlrServiceExecutor);

        return (T) Proxy.newProxyInstance(
                classLoader,
                aClass,
                invocationHandler
        );

    }
}
