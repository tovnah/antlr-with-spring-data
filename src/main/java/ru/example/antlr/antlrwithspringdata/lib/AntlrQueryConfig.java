package ru.example.antlr.antlrwithspringdata.lib;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class AntlrQueryConfig {
    private Object source;
    private String methodName;
    private String query;
    private Class<?> returnType;
}
