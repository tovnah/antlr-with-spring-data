package ru.example.antlr.antlrwithspringdata.lib;

import antlr.gen.AntlrQueryLexer;
import antlr.gen.AntlrQueryParser;
import antlr.gen.AntlrQueryParserVisitorImpl;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.springframework.context.ApplicationContext;

@Slf4j
public class AntlrServiceExecutor {
    public Object execute(AntlrQueryConfig antlrQueryConfig) {
        log.trace("Execute query {}", antlrQueryConfig.getQuery());
        AntlrQueryLexer antlrQueryLexer = new AntlrQueryLexer(CharStreams.fromString(antlrQueryConfig.getQuery()));
        CommonTokenStream tokens = new CommonTokenStream(antlrQueryLexer);
        AntlrQueryParser antlrQueryParser = new AntlrQueryParser(tokens);
        AntlrQueryParser.QueryContext query = antlrQueryParser.query();
        AntlrQueryParserVisitorImpl<Object> visitor = new AntlrQueryParserVisitorImpl<>();
        visitor.setApplicationContext(applicationContext);

        Object result = query.accept(visitor);

        log.trace("Execution result {}", result);

        return result;
    }

    public AntlrServiceExecutor(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    @Getter
    private ApplicationContext applicationContext;
}
