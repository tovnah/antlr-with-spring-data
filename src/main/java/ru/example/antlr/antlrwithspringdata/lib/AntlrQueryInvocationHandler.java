package ru.example.antlr.antlrwithspringdata.lib;

import lombok.Data;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.Map;

@Data
public class AntlrQueryInvocationHandler implements InvocationHandler {
    private Map<String, AntlrQueryConfig> configs;
    private AntlrServiceExecutor antlrServiceExecutor;

    public AntlrQueryInvocationHandler(Map<String, AntlrQueryConfig> configs) {
        this.configs = configs;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        AntlrQueryConfig antlrQueryConfig = configs.get(method.getName());

        Object execute = antlrServiceExecutor.execute(antlrQueryConfig);

        return execute;
    }

}
