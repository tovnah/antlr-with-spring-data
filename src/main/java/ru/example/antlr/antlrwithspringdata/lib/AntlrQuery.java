package ru.example.antlr.antlrwithspringdata.lib;

import org.springframework.stereotype.Component;

import java.lang.annotation.*;

@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Component
public @interface AntlrQuery {
    String value() default "";
}
