package ru.example.antlr.antlrwithspringdata.cfg;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;
import org.springframework.context.annotation.AnnotationBeanNameGenerator;
import org.springframework.core.type.filter.AnnotationTypeFilter;
import org.springframework.stereotype.Component;
import ru.example.antlr.antlrwithspringdata.lib.AntlrService;

import java.util.Set;

@Component
@Slf4j
public class AntlrQueryBeanDefinitionRegistryPostProcessor implements BeanDefinitionRegistryPostProcessor {
    @Override
    @SneakyThrows
    public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry beanDefinitionRegistry) throws BeansException {
        MyClassPathScanningCandidateComponentProvider scsanner = new MyClassPathScanningCandidateComponentProvider(false);
        scsanner.addIncludeFilter(
                new AnnotationTypeFilter(AntlrService.class)
        );
        Set<BeanDefinition> candidateComponents = scsanner.findCandidateComponents("ru.example.antlr");
        for (BeanDefinition candidateComponent : candidateComponents) {
            String beanName = nameGenerator.generateBeanName(candidateComponent, beanDefinitionRegistry);
            beanDefinitionRegistry.registerBeanDefinition(beanName, candidateComponent);
        }
    }

    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory configurableListableBeanFactory) throws BeansException {

    }

    private AnnotationBeanNameGenerator nameGenerator = new AnnotationBeanNameGenerator();
}
