package ru.example.antlr.antlrwithspringdata.cfg;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.InstantiationAwareBeanPostProcessor;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import ru.example.antlr.antlrwithspringdata.lib.AntlrQueryFactory;
import ru.example.antlr.antlrwithspringdata.lib.AntlrService;
import ru.example.antlr.antlrwithspringdata.lib.AntlrServiceExecutor;

@Slf4j
@Component
public class CustomInitializingBean implements InstantiationAwareBeanPostProcessor, BeanFactoryAware {
    @Override
    public Object postProcessBeforeInstantiation(Class<?> beanClass, String beanName) throws BeansException {
        if (beanClass.getAnnotation(AntlrService.class) != null) {
            log.info("Instantiate AntlrQueryService: {}", beanName);
            return AntlrQueryFactory.prepare(beanClass, new AntlrServiceExecutor(applicationContext));
        }

        return null;
    }

    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        this.beanFactory = beanFactory;
    }

    private BeanFactory beanFactory;

    @Autowired
    private ApplicationContext applicationContext;
}
