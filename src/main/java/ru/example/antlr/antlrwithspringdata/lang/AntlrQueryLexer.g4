lexer grammar AntlrQueryLexer;

NL: '\n' | '\r' '\n'? ;
WHITESPACE : (' ' | '\t') -> skip ;
END_OF_EXPR : ';' ;

DOT: '.';
EQEQ: '==' ;
LPAREN: '(' ;
RPAREN: ')' ;
ASSIGNMENT: '=' ;
VAR: 'var' ;
RETURN: 'return' ;

SPRDTCRSR: '#' ;
PARAMCRSR: '$' ;

WORD: [a-zA-Z]+ ;