parser grammar AntlrQueryParser;

@header {
import  java.util.*;
}

options { tokenVocab = AntlrQueryLexer; }

query : expression*  returnexpr;
//returns[Object result]

//var dates = #testEntity.findAll(); filtered = filter(dates.getField() == $param); return filtered;
expression: ((newVariable | variable) ASSIGNMENT (command | definition)  END_OF_EXPR );

parameter: PARAMCRSR WORD ;
dataEntity: SPRDTCRSR WORD ;

definition: (parameter | dataEntity | variable) methodInvoke ;

command: WORD LPAREN condition RPAREN;

condition: (definition | variable | parameter) EQEQ (definition | variable | parameter);
returnexpr: RETURN variable;

variable: WORD ;
newVariable: VAR? WORD ;
methodInvoke: DOT WORD LPAREN RPAREN ;