package ru.example.antlr.antlrwithspringdata;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;

import java.util.List;

@Slf4j
@SpringBootApplication
public class AntlrWithSpringDataApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext run = SpringApplication.run(AntlrWithSpringDataApplication.class, args);

        TestEntityRepository repository = run.getBean(TestEntityRepository.class);
        TestEntity testEntity = new TestEntity();
        testEntity.setField("Hello world");
        repository.save(testEntity);

        AntlrExampleExecutor bean = run.getBean(AntlrExampleExecutor.class);
        List<TestEntity> answer = bean.filterAnswer("");

        log.info("Answer {}", answer);
    }

}
